#!/usr/bin/perl
use strict;
use warnings;
use Nagios::Monitoring::Plugin;
use Nagios::SNMP;
    
my $ng = new Nagios::Monitoring::Plugin(
    usage => "Usage: %s -H host [-c N] [-t TIMEOUT] [-w N] [--host=HOST] [--critical=N] [--warning=N] ENDPOINT",
    version => $Nagios::SNMP::VERSION,
    blurb => qq{Checks OWS accessability from a remote server.},
    #    extra => ''
);

$ng->add_arg(spec => 'host|H=s',
	     help => 'Host name or IP address',
	     required => 1);
$ng->add_arg(spec => 'warning|w=i',
	     help => '-w, --warning=N   set warning threshold (%)');
$ng->add_arg(spec => 'critical|c=i',
	     help => '-c, --critical=N  set critical threshold (%)');
$ng->getopts;

my $endpoint = shift @ARGV or
    $ng->nagios_die("usage error: endpoint name must be given");
$ng->nagios_die("usage error: extra arguments") if @ARGV;

my $res = eval {
    Nagios::SNMP->new(DestHost => $ng->opts->get('host'),
                      timeout => $ng->opts->get('timeout'))
	->get_table_row('OWS-STATS::endpointTable', 'endpointName', $endpoint,
			response_time  => 'OWS-STATS::endpointResponseTime',
                        url            => 'OWS-STATS::endpointURL');
};
if ($@) {
    my ($mesg) = split /\n/, $@;
    $ng->nagios_die($mesg);
};

$ng->set_thresholds(warning => $ng->opts->get("warning"),
		    critical => $ng->opts->get("critical"));

$ng->add_perfdata(
    label => "responsetime",
    uom => 'ms',
    value => $res->response_time,
    threshold => $ng->threshold
);

$ng->nagios_exit(
    return_code => $ng->check_threshold(check => $res->response_time),
    message => $res->response_time."ms, URL ".$res->url
);

