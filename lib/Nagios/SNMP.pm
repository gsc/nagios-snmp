package Nagios::SNMP;
use strict;
use warnings;
use Nagios::Monitoring::Plugin;
use SNMP;
use File::Basename;
use File::Spec;
use Storable qw(freeze thaw fd_retrieve store_fd);
use GDBM_File;
use Carp;
use SNMP;
use Digest::SHA1;
use Fcntl qw(:DEFAULT :flock SEEK_SET);
use File::Path qw(make_path);
use DateTime;

our $VERSION = '0.01';
our $statedir = '/var/state/nagios-snmp';

sub state_file_path {
    my $class = shift;
    my $filename = File::Spec->catfile($statedir, $<, @_);
    my $dir = dirname($filename);
    unless (-d $dir) {
	make_path($dir, {error => \my $err});
	if (@$err) {
	    my ($f, $m) = %{$err->[0]};
	    croak "Error creating $dir: $f: $m"
	}
    }
    return $filename;
}

# new(keys)
sub new {
    my $class = shift;
    local %_ = @_;
    my $snmp = new SNMP::Session(%_);
    croak "Can't connect to $_{DestHost}" unless $snmp;
    my $filename = $class->state_file_path('db', $snmp->{DestHost});
    tie my %hash, 'GDBM_File', $filename, &GDBM_WRCREAT, 0640;
    bless {
	snmp => $snmp,
	file => $filename,
        hash => \%hash,
    }, $class;
}

sub DESTROY {
    my $self = shift;
    untie %{$self->{hash}};
}

#   A date-time specification.
#
#   field  octets  contents                  range
#   -----  ------  --------                  -----
#     1      1-2   year*                     0..65536
#     2       3    month                     1..12
#     3       4    day                       1..31
#     4       5    hour                      0..23
#     5       6    minutes                   0..59
#     6       7    seconds                   0..60
#                  (use 60 for leap-second)
#     7       8    deci-seconds              0..9
#     8       9    direction from UTC        '+' / '-'
#     9      10    hours from UTC*           0..13
#    10      11    minutes from UTC          0..59
#
# * Notes:
#   - the value of year is in network-byte order
#   - daylight saving time in New Zealand is +13

sub decode_time {
    my ($pt) = @_;
    my ($yr, $mon, $day, $hr, $min, $sec, $dec, $dir, $tzhr, $tzmin) =
	unpack "nCCCCCCCCC", $pt;
    return DateTime->new(
	year       => $yr,
	month      => $mon,	
	day        => $day,
	hour       => $hr,
	minute     => $min,
	second     => $sec,
	time_zone  => sprintf("%c%02d%02d", $dir, $tzhr, $tzmin)
    )->epoch * 10 + $dec;
}

sub sysdate {
    my $self = shift;
    my $res = $self->{snmp}->get([['HOST-RESOURCES-MIB::hrSystemDate.0']])
	or croak "Can't get system date: $self->{snmp}{ErrorStr}";
    croak "Can't get system date: no such insttance"
	if $res eq 'NOSUCHINSTANCE';
    return decode_time($res);
}

sub starttime {
    my $self = shift;
    my @res = $self->{snmp}->get([['HOST-RESOURCES-MIB::hrSystemDate.0'],
	 		          ['SNMPv2-MIB::sysUpTime.0']])
	or croak "Can't get uptime: $self->{snmp}{ErrorStr}";
    croak "Can't get system date: no such insttance"
	if $res[0] eq 'NOSUCHINSTANCE';
    croak "Can't get uptime: no such insttance"
	if $res[1] eq 'NOSUCHINSTANCE';
    
    return decode_time($res[0]) - int($res[1]/10);
}

# get_index(TABLE, COLUMN, [ value => VALUE ], [ indexer => SUB ] )
sub get_index {
    my ($self, $table, $column, %args) = @_;
    my $start_time = $self->starttime;
    my $key = $table . '.' . $column;
    my $val;

    if (exists($self->{hash}{$key})) {
	$val = thaw($self->{hash}{$key});
	if (abs($val->{start_time} - $start_time) > 9) {
	    $val = undef;
	}
    }

    unless ($val) {
	my $res = $self->{snmp}->gettable($table, columns => [ $column ]);
	if (!$res) {
	    croak "can't get table $table: ". $self->{snmp}{ErrorStr};
	}

	my $indexer = $args{indexer};
	if ($indexer) {
	    croak "indexer must be a coderef" unless  ref($indexer) eq 'CODE';
	} else {
	    $indexer = sub {
		my ($res, $column) = @_;
		return { map { $res->{$_}{$column} => $_ } keys %$res }
	    }
	}

	$val = {
	    start_time => $start_time,
	    index => &{$indexer}($res, $column)
	};
	$self->{hash}{$key} = freeze($val);
    }

    if ($args{value}) {
	$val = $val->{index}{$args{value}};
    }

    return $val
}

sub get_values {
    my ($self, @varlist) = @_;
    my @res = $self->{snmp}->get([map { [$_] } @varlist]);
    if (my @unres = grep { $res[$_] =~ /^NOSUCH/ } 0..$#res) {
	croak "the following instances are not resolved: "
	    . join(', ', map { $varlist[$_] } @unres);
    }
    @res
}

# get(NAME => OID...)
sub get {
    my ($self, @oids) = @_;
    my @v = $self->{snmp}->get([map {[$oids[$_]]} grep {$_ & 1} 1..$#oids]);
    croak "can't get ".
	join(',', map { $oids[$_] } grep {$_ & 1} 1..$#oids).
	': '.$self->{snmp}{ErrorStr}
	unless @v;
    my $ret = {map { $oids[$_] => $v[$_/2] } grep {($_ & 1) == 0} 0..$#oids};
    if ($self->{cachefile}) {
	sysopen(my $fd, $self->{cachefile}, O_RDWR|O_CREAT, 0644)
	    or croak "can't open cache file $self->{cachefile}: $!";
	flock($fd, LOCK_EX)
	    or croak "can't lock cache file $self->{cachefile}: $!";
	$ret->{__timestamp} = time;
	if ((stat($fd))[7] == 0) {
	    # New file
	    store_fd($ret, $fd);
	    @{$ret}{keys %$ret} = (0) x keys %$ret;
	    $ret->{__initializing} = 1;
	} else {
	    my $prev = fd_retrieve($fd);
	    seek $fd, 0, SEEK_SET;
	    store_fd($ret, $fd);
	    my $dt = $ret->{__timestamp} - $prev->{__timestamp};
	    map { $ret->{$_} = ($ret->{$_} - $prev->{$_}) / $dt } keys %$ret;
	    if (grep { $_ < 0 } values %$ret) {
		@{$ret}{keys %$ret} = (0) x keys %$ret;
	        $ret->{__initializing} = 1;
	    }
	}
	flock($fd, LOCK_UN);
	close $fd;
    }
    if (my @unres = (grep { $ret->{$_} =~ m/^NOSUCH/ } keys %$ret)) {
	my %arg = @oids;
	croak "the following instances are not resolved: "
	    . join(', ', map { $arg{$_} } @unres);
    }
    bless $ret, 'Nagios::SNMP::Reply'
}

sub get_table_row {
    my ($self, $table, $column, $value, @oids) = @_;
    my $idx = $self->get_index($table, $column, value => $value);
    croak "no such index" unless defined $idx;
    $self->get(map { ($_ & 1) ? $oids[$_] . ".$idx" : $oids[$_]  } 0..$#oids);
}

sub error {
    my $self = shift;
    return $self->{snmp}{ErrorStr} || "no such index";
}

sub set_cache {
    my $self = shift;
    if (@_) {
	my $d = Digest::SHA1->new;
        map { $d->add($_) } @_;
        my $digest = $d->hexdigest;
        $self->{cachefile} = __PACKAGE__->state_file_path('cache', $digest);
        return $self->{cachefile};
    } else {
	delete $self->{cachefile};
    }
}

package Nagios::SNMP::Reply;
use strict;
use warnings;
use Carp;

# new(ATTRS...)
sub new {
    my $class = shift;
    my %s;
    @s{@_} = (undef) x @_;
    bless \%s, $class;
}

sub timestamp { return shift->attr('__timestamp') }
sub initializing { return shift->attr('__initializing') }

sub attr {
    my ($self, $attr) = @_;
    if (@_ == 3) {
	$self->{$attr} = $_[2];
    }
    return $self->{$attr};
}

our $AUTOLOAD;

sub AUTOLOAD {
    my $self = shift;
    (my $attr = $AUTOLOAD) =~ s/^.*:://;
    croak "no such attribute: $attr" unless exists($self->{$attr});
    $self->attr($attr, @_);
}

sub DESTROY { }  

1;
